window.App = new App();
$(document).ready(window.App.init);

function App()
{
	var gamesCollection;

	return {
		init: function()
		{
			var gamesView = new window.View.Games({ el: $('.games') }),
				wishListView = new window.View.WishList({ el: $('.wish-list') });

			wishListView.render();
		},
		getGamesCollection: function()
		{
			if (!gamesCollection)
			{
				gamesCollection = new window.Collection.Games();
			}
			return gamesCollection;
		}
	}
}

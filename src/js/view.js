window.View = {
	Game: Backbone.View.extend({
		tagName: "div",
		className: "game",
		events: {
			'click .game__button': "toggleWish"
		},
		initialize: function() {
			this.model.bind('change:inWishList', this.onWishToggled, this)
		},
		onWishToggled: function() {
			var addBtn = this.$('.game__button_add');
				removeBtn = this.$('.game__button_remove');

			addBtn.toggleClass('game__button_hidden');
			removeBtn.toggleClass('game__button_hidden');
		},
		toggleWish: function() {
			this.model.set('inWishList', !this.model.get('inWishList'));
		},
		render: function() {
			this.template = window.Templates['game.html'](this.model.attributes);
			this.$el.html(this.template);
			this.$el.attr('id', this.model.id);

			this.$el.draggable({
				helper: 'clone',
				tolerance: 'pointer',
				delay: 200,
				scroll: false
			});        
			return this;
		}
	}),
	Games: Backbone.View.extend({
		initialize: function() {
			this.games = new window.App.getGamesCollection();
			this.games.bind('add', this.addOne, this);
			this.games.bind('reset', this.addAll, this);
			this.games.load();
		},
		addAll: function() {
			this.games.each(this.addOne.bind(this));
		},
		addOne: function(game) {
			var view = new window.View.Game({model: game});
			this.$el.append(view.render().el);
		}
	}),
	WishGame: Backbone.View.extend({
		tagName: "div",
		className: "wish-game",
		events: {
			'click': 'removeFromWish'
		},
		removeFromWish: function() {
			this.model.set('inWishList', false);
		},
		initialize: function() {
			this.model.bind('change:inWishList', this.onInWishChanged, this);
		},
		onInWishChanged: function() {
			if (!this.model.get('inWishList'))
				this.destroy();
		},
		destroy: function () {
			this.remove();
			this.unbind();
			this.undelegateEvents();
			this.model.unbind(null, null, this);
		},
		render: function() {
			this.template = window.Templates['wish_game.html'](this.model.attributes);
			this.$el.html(this.template);

			return this;
		}
	}),
	WishList: Backbone.View.extend({
		events: {
			'click .wish-list__clear': 'clearGames'
		},
		initialize: function() {
			this.games = new window.App.getGamesCollection();
			this.games.bind('change:inWishList', this.onInWishChanged, this);
		},
		clearGames: function() {
			var gamesInWishList = this.games.where({ inWishList: true });
			_.each(gamesInWishList, function(model) {
				model.set('inWishList', false);
			})
		},
		addGame: function(model) {
			wishGameView = new window.View.WishGame({model: model});
			this.gamesNode.append(wishGameView.render().el);
		},
		onInWishChanged: function(model) {
			this.amountNode.html(this.calcAmount());
			if (model.get('inWishList'))
				this.addGame(model);
		},
		calcAmount: function () {
			var gamesInWishList = this.games.where({ inWishList: true }),
				result = _.reduce(gamesInWishList, function (sum, game) {
					return sum + game.get('price')
				}, 0);

			return result
		},
		getModelByNode: function(node) {
			var modelId = node.attr('id');
			return this.games.get({ id: modelId});
		},
		prepareDrag: function() {
			this.draggableNode = this.$('.wish-list__content-wrapper');
			this.draggableNode.droppable({
				accept: '.game',

				drop: function(event, ui) {
	            	var model = this.getModelByNode(ui.draggable);

	            	if (!model.get('inWishList'))
	            		model.set('inWishList', true);

	      		}.bind(this),

	      		activate: function(event, ui) {
	      			var model = this.getModelByNode(ui.draggable);
	      			if (!model.get('inWishList'))
						this.draggableNode.addClass('wish-list__content-wrapper-drag-active');

				}.bind(this),

				deactivate: function() {
					this.draggableNode.removeClass('wish-list__content-wrapper-drag-active');

				}.bind(this)
			})
		},
		render: function() {
			this.template = window.Templates['wish_list.html']({amount: this.calcAmount()});
			this.$el.html(this.template);
			this.gamesNode = this.$('.wish-list__games');
			this.amountNode = this.$('.wish-list__amount');

			this.prepareDrag();

			return this;
		}
	})
}
/**
 * @requires model.js
 */

window.Collection = {
	Games: Backbone.Collection.extend({
		localStorage: new Store("games-wish-list"),
		model: window.Model.Game, 
		url: 'json/products.json',
		initialize: function() {
			this.bind('change:inWishList', this.onWishChanged, this);
		},
		onWishChanged: function (model) {
			model.get('inWishList') ? model.save() : model.removeFromStorage()
		},
		load: function () {
			//сначала получаем дефолтные модели с сервера
			this.fetch({ 
				ajaxSync: true,
				parse: true,

				success: function() {
					//и мерджим их с моделями в localStorage
					this.fetch({ 
						parse: false,
						remove: false,
						
						success: function() {
							this.trigger('loaded');
						}.bind(this)
					});
				}.bind(this)
			});
		},
		parse: function(products) {
			var result = [],
				gameJson, id;

			for (id in products) {
				gameJson = products[id];
				gameJson.id = id;
				result.push(gameJson);
			}

			return result;
		}
	})
}
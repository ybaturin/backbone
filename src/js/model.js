window.Model = {
	Game: Backbone.Model.extend({
		defaults: function() {
			return {
				name: '',
				price: '',
				cover: '',
				inWishList: false
			}
		},
		removeFromStorage: function() {
			this.sync('delete', this);
		}
	})
}
var gulp = require('gulp'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    less = require('gulp-less'),
    cssmin = require('gulp-minify-css'),
    concat = require('gulp-concat'),
    template = require('gulp-template-compile'),
    resolveDependencies = require('gulp-resolve-dependencies'),
    rimraf = require('rimraf');

var path = {
    build: {
        html: 'build/',
        js: 'build/js/',
        static: 'build/static/',
        css: 'build/css/',
        json: 'build/json/'
    },
    src: {
        html: 'src/*.html',
        js: 'src/js/*.js',
        templates: 'src/templates/*.html',
        style: 'src/style/*.less',
        static: 'src/static/**/*.*',
        json: 'src/json/*.json'
    },
    watch: {
        html: 'src/*.html',
        js: 'src/js/*.js',
        style: 'src/style/*.less',
        templates: 'src/templates/*.html',
        json: 'src/json/*.json'
    },
    clean: './build'
};

gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

gulp.task('html:build', function () {
    gulp.src(path.src.html).pipe(gulp.dest(path.build.html))
});

gulp.task('static:copy', function () {
    gulp.src(path.src.static)
        .pipe(gulp.dest(path.build.static))
});

gulp.task('json:copy', function () {
    gulp.src(path.src.json)
        .pipe(gulp.dest(path.build.json))
});

gulp.task('templates:build', function () {
    gulp.src(path.src.templates)
        .pipe(template({namespace: 'Templates'}))
        .pipe(concat('templates.js'))
        .pipe(gulp.dest(path.build.js))
});

gulp.task('js:build', function () {
    gulp.src(path.src.js)
        .pipe(resolveDependencies())
        .pipe(uglify())
        .pipe(concat('code.js'))
        .pipe(gulp.dest(path.build.js))
});

gulp.task('style:build', function () {
    gulp.src(path.src.style) 
        .pipe(concat('all.less'))
        .pipe(less())
        .pipe(prefixer())
        .pipe(cssmin())
        .pipe(gulp.dest(path.build.css))
});

gulp.task('build', [
    'html:build',
    'js:build',
    'style:build',
    'static:copy',
    'json:copy',
    'templates:build'
]);

gulp.task('watch', function(){
    gulp.watch(path.watch.html, ['html:build']);
    gulp.watch(path.watch.style, ['style:build']);
    gulp.watch(path.watch.js, ['js:build']);
    gulp.watch(path.watch.templates, ['templates:build']);
    gulp.watch(path.watch.json, ['json:copy']);
});


gulp.task('default', ['build', 'watch']);